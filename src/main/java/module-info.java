module Processamento.Imagem.main {

    requires javafx.controls;
    requires javafx.fxml;

    opens org.processamento to javafx.fxml;

    exports org.processamento;

}