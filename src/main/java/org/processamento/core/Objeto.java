package org.processamento.core;

import javafx.scene.input.MouseEvent;
import org.processamento.dominio.Ponto;

import java.util.HashSet;
import java.util.Set;

public abstract class Objeto implements Compositor<Objeto, Set<Ponto>> {

    private final Set<Observador<Set<Ponto>>> observadores;

    private boolean modoContinuo;
    private boolean modoPoligono;

    protected Objeto() {
        observadores = new HashSet<>();
        modoContinuo = false;
        modoPoligono = false;
    }

    public final void processarEvento(MouseEvent mouseEvent) {

        Ponto pontoMouse = new Ponto(mouseEvent.getX(), mouseEvent.getY());

        adicionarPonto(pontoMouse).
                seProntoParaProcessar(() -> {

                    Set<Ponto> pontos = construir();

                    publicar(pontos)
                            .seModoUnitario()
                            .seModoPoligono();

                });

    }

    public final void continua() {
        modoContinuo = true;
        modoPoligono = false;
    }

    public final void unitaria() {
        modoContinuo = false;
        modoPoligono = false;
        limparObjetos();
    }

    public final void poligono() {
        modoPoligono = true;
        modoContinuo = false;
    }

    private Objeto seModoUnitario() {
        if (!modoContinuo && !modoPoligono) {
            limparObjetos();
        }
        return this;
    }

    private void seModoPoligono() {
        if (modoPoligono) {
            processarModoPoligono();
        }
    }

    @Override
    public final Objeto publicar(Set<Ponto> pontos) {

        observadores
                .parallelStream()
                .forEach(observador -> observador.atualizar(pontos));

        return this;

    }

    @Override
    public final Objeto adicionarObservador(Observador<Set<Ponto>> observador) {
        observadores.add(observador);
        return this;
    }

    protected abstract Set<Ponto> construir();

    protected abstract void seProntoParaProcessar(Acao acao);

    protected abstract Objeto adicionarPonto(Ponto ponto);

    protected abstract void limparObjetos();

    protected abstract void processarModoPoligono();

}
