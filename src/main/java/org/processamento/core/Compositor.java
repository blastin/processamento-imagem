package org.processamento.core;

public interface Compositor<R,T> {

    R publicar(T t);

    R adicionarObservador(Observador<T> observador);

}
