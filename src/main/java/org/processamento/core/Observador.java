package org.processamento.core;

public interface Observador<T> {

    void atualizar(T t);

}
