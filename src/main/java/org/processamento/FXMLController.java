package org.processamento;

import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import org.processamento.core.Objeto;
import org.processamento.core.Observador;
import org.processamento.dominio.*;
import org.processamento.observador.Desenho;
import org.processamento.observador.Espelho;
import org.processamento.observador.Pilha;
import org.processamento.servico.ContextoGrafico;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;

public class FXMLController implements Initializable {

    public Canvas canvasDesenho;

    public Canvas canvasEspelho;

    private Objeto objeto;

    private Observador<Set<Ponto>> desenho;

    private Observador<Set<Ponto>> espelho;

    private Pilha pilhaDePontos;

    private boolean continua;

    private boolean poligono;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        desenho = new Desenho(canvasDesenho);

        espelho = new Espelho(canvasDesenho, canvasEspelho);

        pilhaDePontos = new Pilha()
                .adicionarObservador(desenho)
                .adicionarObservador(espelho);

        eventoLimpar();

    }

    public void eventoPonto() {

        System.out.println("Modo desenho ponto");

        objeto = new Ponto()
                .adicionarObservador(desenho)
                .adicionarObservador(espelho)
                .adicionarObservador(pilhaDePontos);

    }

    public void eventoReta() {

        System.out.println("Modo desenho reta");

        objeto = new Reta()
                .adicionarObservador(desenho)
                .adicionarObservador(espelho)
                .adicionarObservador(pilhaDePontos);

        funcaoContinua();
        funcaoPoligono();
    }


    public void eventoCirculo() {

        System.out.println("Modo desenho circulo");

        objeto = new Circulo()
                .adicionarObservador(desenho)
                .adicionarObservador(espelho)
                .adicionarObservador(pilhaDePontos);

        funcaoContinua();
        funcaoPoligono();

    }

    public void eventoTriangulo() {

        System.out.println("Modo desenho Triangulo");

        objeto = new Triangulo()
                .adicionarObservador(desenho)
                .adicionarObservador(espelho)
                .adicionarObservador(pilhaDePontos);

        funcaoContinua();
        funcaoPoligono();

    }

    public void eventoLimpar() {

        System.out.println("Limpando Tela");

        continua = false;

        objeto = new ObjetoNulo();

        ContextoGrafico.limnparCanvas(canvasDesenho);
        ContextoGrafico.limnparCanvas(canvasEspelho);

        pilhaDePontos.limpar();

    }

    public void eventoDesenhar(MouseEvent mouseEvent) {

        System.out.println("Desenhando em tela");

        objeto.processarEvento(mouseEvent);

    }

    public void eventoFuncaoContinuo() {

        System.out.println("Modo desenho continuo");

        objeto.continua();
        continua = true;
        poligono = false;
    }

    public void eventoFuncaoUnitaria() {

        System.out.println("Modo desenho unitário");

        objeto.unitaria();
        continua = false;
        poligono = false;
    }

    public void eventoVoltar() {

        System.out.println("Limpando ultima função");

        pilhaDePontos.pop();

        pilhaDePontos.redesenhar(Set.of(canvasDesenho, canvasEspelho));

    }

    private void funcaoContinua() {
        if (continua) {
            objeto.continua();
        }
    }

    private void funcaoPoligono() {
        if (poligono) {
            objeto.poligono();
        }
    }


    public void eventoFuncaoPoligono(MouseEvent mouseEvent) {

        System.out.println("Modo desenho poligono");

        objeto.poligono();

        poligono = true;
    }
}
