package org.processamento.observador;

import javafx.scene.canvas.Canvas;
import org.processamento.core.Observador;
import org.processamento.dominio.Dimensao;
import org.processamento.dominio.Ponto;
import org.processamento.servico.ContextoGrafico;

import java.util.Set;

public class Desenho implements Observador<Set<Ponto>> {

    private final Canvas canvas;

    public Desenho(Canvas canvas) {
        this.canvas = canvas;
    }

    @Override
    public void atualizar(Set<Ponto> pontos) {

        pontos
                .forEach(ponto -> ContextoGrafico
                        .desenharPixelEmCanvas(
                                canvas,
                                ponto,
                                new Dimensao(4, 4)));

    }
}
