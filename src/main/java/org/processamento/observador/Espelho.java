package org.processamento.observador;

import javafx.scene.canvas.Canvas;
import org.processamento.core.Observador;
import org.processamento.dominio.Dimensao;
import org.processamento.dominio.Ponto;
import org.processamento.dominio.TransicaoTela;
import org.processamento.servico.ContextoGrafico;

import java.util.Set;
import java.util.function.Function;

public class Espelho implements Observador<Set<Ponto>> {

    private final Canvas canvasEspelho;

    private final Function<Ponto, Ponto> transicaoTela;

    public Espelho(Canvas canvasDesenho, Canvas canvasEspelho) {
        this.canvasEspelho = canvasEspelho;
        this.transicaoTela = new TransicaoTela(canvasDesenho, canvasEspelho);
    }

    @Override
    public void atualizar(Set<Ponto> pontos) {

        pontos
                .stream()
                .map(transicaoTela)
                .forEach(ponto -> ContextoGrafico
                        .desenharPixelEmCanvas(
                                canvasEspelho,
                                ponto,
                                new Dimensao(4, 4)));

    }
}
