package org.processamento.observador;

import javafx.scene.canvas.Canvas;
import org.processamento.core.Compositor;
import org.processamento.core.Observador;
import org.processamento.dominio.Ponto;
import org.processamento.servico.ContextoGrafico;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class Pilha implements Observador<Set<Ponto>>, Compositor<Pilha, Set<Ponto>> {

    private final Set<Observador<Set<Ponto>>> observadores;

    private final Stack<Set<Ponto>> pilhaDePontos;

    public Pilha() {

        pilhaDePontos = new Stack<>();

        observadores = new HashSet<>();
    }

    @Override
    public void atualizar(Set<Ponto> pontos) {
        this.pilhaDePontos.add(pontos);
    }

    public void pop() {

        if (pilhaDePontos.empty()) {
            return;
        }

        pilhaDePontos.pop();

    }

    public void redesenhar(Set<Canvas> canvasSet) {

        canvasSet.forEach(ContextoGrafico::limnparCanvas);

        final HashSet<Set<Ponto>> sets = new HashSet<>(pilhaDePontos);

        sets.forEach(this::publicar);

    }

    @Override
    public Pilha publicar(Set<Ponto> pontos) {

        observadores
                .forEach(observador -> observador.atualizar(pontos));

        return this;
    }

    @Override
    public Pilha adicionarObservador(Observador<Set<Ponto>> observador) {
        observadores.add(observador);
        return this;
    }

    public void limpar() {
        pilhaDePontos.clear();
    }
}
