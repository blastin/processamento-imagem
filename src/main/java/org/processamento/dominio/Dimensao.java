package org.processamento.dominio;

public class Dimensao {

    private final Integer comprimento;

    private final Integer altura;

    public Dimensao(Integer comprimento, Integer altura) {
        this.comprimento = comprimento;
        this.altura = altura;
    }

    public Integer getComprimento() {
        return comprimento;
    }

    public Integer getAltura() {
        return altura;
    }

}
