package org.processamento.dominio;

import org.processamento.core.Acao;
import org.processamento.core.Objeto;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Triangulo extends Objeto {

    private Map<String, Ponto> pontoMap;

    private static final String PONTO_A = "A";
    private static final String PONTO_B = "B";
    private static final String PONTO_C = "C";

    public Triangulo() {
        pontoMap = new HashMap<>();
    }

    @Override
    protected Set<Ponto> construir() {

        final Ponto a = pontoMap.get(PONTO_A);
        final Ponto b = pontoMap.get(PONTO_B);
        final Ponto c = pontoMap.get(PONTO_C);

        Set<Ponto> pontos;

        Reta ab = new Reta(a, b);
        pontos = ab.construir();

        Reta ac = new Reta(a, c);
        pontos.addAll(ac.construir());

        Reta bc = new Reta(b, c);
        pontos.addAll(bc.construir());

        return pontos;

    }

    @Override
    protected void seProntoParaProcessar(Acao acao) {

        if (pontoMap.size() == 3) {
            acao.realizar();
        }

    }

    @Override
    protected Objeto adicionarPonto(Ponto ponto) {

        if (!pontoMap.containsKey(PONTO_A)) {
            pontoMap.put(PONTO_A, ponto);
        } else if (!pontoMap.containsKey(PONTO_B)) {
            pontoMap.put(PONTO_B, ponto);
        } else {
            pontoMap.put(PONTO_C, ponto);
        }

        return this;
    }

    @Override
    protected void limparObjetos() {
        pontoMap.clear();
    }

    @Override
    protected void processarModoPoligono() {

        final Ponto ponto = pontoMap.get(PONTO_C);

        limparObjetos();

        pontoMap.put(PONTO_C, ponto);

    }

}
