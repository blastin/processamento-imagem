package org.processamento.dominio;

import org.processamento.core.Acao;
import org.processamento.core.Objeto;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Circulo extends Objeto {

    private Map<String, Ponto> pontoMap;

    private static final String PONTO_A = "A";
    private static final String PONTO_B = "B";

    public Circulo() {
        pontoMap = new HashMap<>();
    }

    @Override
    public Set<Ponto> construir() {

        Set<Ponto> pontos = new HashSet<>();

        Ponto a = pontoMap.get(PONTO_A);

        Ponto b = pontoMap.get(PONTO_B);

        double raio = Math.sqrt(Math.pow(a.moduloX(b), 2) + Math.pow(a.moduloY(b), 2));

        for (double taxaAngular = 0.0; taxaAngular < 360; taxaAngular += 0.1) {

            double y = raio * Math.sin(taxaAngular) + a.yParaInt();

            double x = raio * Math.cos(taxaAngular) + a.xParaInt();

            pontos.add(new Ponto(x, y));

        }

        return pontos;

    }

    @Override
    protected void seProntoParaProcessar(Acao acao) {

        if (pontoMap.size() == 2) {
            acao.realizar();
        }

    }

    @Override
    protected Objeto adicionarPonto(Ponto ponto) {

        if (!pontoMap.containsKey(PONTO_A)) {
            pontoMap.put(PONTO_A, ponto);
        } else {
            pontoMap.put(PONTO_B, ponto);
        }

        return this;
    }

    @Override
    protected void limparObjetos() {

        pontoMap.clear();

    }

    @Override
    protected void processarModoPoligono() {

        final Ponto ponto = pontoMap.get(PONTO_B);

        limparObjetos();

        pontoMap.put(PONTO_A, ponto);

    }

}
