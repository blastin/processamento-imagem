package org.processamento.dominio;

import org.processamento.core.Acao;
import org.processamento.core.Objeto;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Reta extends Objeto {

    private Map<String, Ponto> pontoMap;

    private static final String PONTO_A = "A";
    private static final String PONTO_B = "B";

    public Reta() {
        pontoMap = new HashMap<>();
    }

    Reta(Ponto a, Ponto b) {
        pontoMap = new HashMap<>();
        pontoMap.put(PONTO_A, a);
        pontoMap.put(PONTO_B, b);
    }

    @Override
    public Set<Ponto> construir() {

        Set<Ponto> pontos;

        Ponto a = pontoMap.get(PONTO_A);

        Ponto b = pontoMap.get(PONTO_B);

        if (a.moduloX(b) <= 20) {

            pontos = retaComModuloXMenorQue20(a, b);

        } else {
            pontos = retaNormal(a, b);
        }

        return pontos;


    }

    @Override
    protected void seProntoParaProcessar(Acao acao) {

        if (pontoMap.size() == 2) {
            acao.realizar();
        }
    }

    @Override
    protected Objeto adicionarPonto(Ponto ponto) {

        if (!pontoMap.containsKey(PONTO_A)) {
            pontoMap.put(PONTO_A, ponto);
        } else {
            pontoMap.put(PONTO_B, ponto);
        }

        return this;
    }

    @Override
    protected void limparObjetos() {

        pontoMap.clear();

    }

    @Override
    protected void processarModoPoligono() {

        final Ponto ponto = pontoMap.get(PONTO_B);

        limparObjetos();

        pontoMap.put(PONTO_A, ponto);

    }

    private Set<Ponto> retaComModuloXMenorQue20(Ponto a, Ponto b) {

        Set<Ponto> pontos = new HashSet<>();

        Ponto maior = a.obterPontoMaiorEmRelacaoEixoY(b);

        Ponto menor = a.obterPontoMenorEmRelacaoEixoY(b);

        for (int y = menor.yParaInt(); y < maior.yParaInt(); y++) {

            pontos.add(new Ponto(menor.xParaInt(), y));

        }

        return pontos;

    }

    private Set<Ponto> retaNormal(Ponto a, Ponto b) {

        Set<Ponto> pontos = new HashSet<>();

        Ponto maior = a.obterPontoMaiorEmRelacaoEixoX(b);

        Ponto menor = a.obterPontoMenorEmRelacaoEixoX(b);

        Float coeficienteAngular = maior.coeficienteAngular(menor);

        for (int x = menor.xParaInt(); x < maior.xParaInt(); x++) {

            float y = coeficienteAngular * (x - menor.xParaInt()) + menor.yParaInt();

            pontos.add(new Ponto(x, y));

        }

        return pontos;

    }

}
