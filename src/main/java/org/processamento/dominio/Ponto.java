package org.processamento.dominio;

import org.processamento.core.Acao;
import org.processamento.core.Objeto;

import java.util.Set;

public class Ponto extends Objeto {

    private Integer x;

    private Integer y;

    public Ponto() {
    }

    public Ponto(double x, double y) {
        this.x = Double.valueOf(x).intValue();
        this.y = Double.valueOf(y).intValue();
    }

    public int xParaInt() {
        return x;
    }

    public int yParaInt() {
        return y;
    }

    Ponto obterPontoMaiorEmRelacaoEixoX(Ponto ponto) {
        return (x - ponto.xParaInt()) > 0 ? this : ponto;
    }

    Ponto obterPontoMenorEmRelacaoEixoX(Ponto ponto) {
        return obterPontoMaiorEmRelacaoEixoX(ponto) == this ? ponto : this;
    }

    Ponto obterPontoMaiorEmRelacaoEixoY(Ponto ponto) {
        return (y - ponto.yParaInt()) > 0 ? this : ponto;
    }

    Ponto obterPontoMenorEmRelacaoEixoY(Ponto ponto) {
        return obterPontoMaiorEmRelacaoEixoY(ponto) == this ? ponto : this;
    }

    Float coeficienteAngular(Ponto b) {
        return (y - b.yParaInt() * 1f) / (x - b.xParaInt() * 1f);
    }

    Integer moduloX(Ponto ponto) {
        return x > ponto.xParaInt() ? x - ponto.xParaInt() : ponto.xParaInt() - x;
    }

    Integer moduloY(Ponto ponto) {
        return y > ponto.yParaInt() ? y - ponto.yParaInt() : ponto.yParaInt() - y;
    }

    @Override
    public String toString() {
        return "Ponto [x=" + x + ", y=" + y + "]";
    }

    @Override
    protected Set<Ponto> construir() {
        return Set.of(new Ponto(x, y));
    }

    @Override
    protected void seProntoParaProcessar(Acao acao) {
        acao.realizar();
    }

    @Override
    protected Objeto adicionarPonto(Ponto ponto) {

        x = ponto.x;
        y = ponto.y;

        return this;
    }

    @Override
    protected void limparObjetos() {
    }

    @Override
    protected void processarModoPoligono() {

    }
}
