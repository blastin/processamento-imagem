package org.processamento.dominio;

import javafx.scene.canvas.Canvas;

import java.util.function.Function;

public class TransicaoTela implements Function<Ponto, Ponto> {

    private final Canvas canvasDesenho;
    private final Double resolucaoEixoX;
    private final Double resolucaoEixoY;

    public TransicaoTela(Canvas canvasDesenho, Canvas canvasEspelho) {
        resolucaoEixoX = canvasEspelho.getWidth();
        resolucaoEixoY = canvasEspelho.getHeight();
        this.canvasDesenho = canvasDesenho;
    }

    @Override
    public Ponto apply(Ponto ponto) {
        return new
                Ponto
                (calculoParaEixoX(ponto) * resolucaoEixoX,
                        calculoParaEixoY(ponto) * resolucaoEixoY
                );
    }

    private Double calculoParaEixoX(Ponto ponto) {
        return ponto.xParaInt() / canvasDesenho.getWidth();
    }

    private Double calculoParaEixoY(Ponto ponto) {
        return ponto.yParaInt() / canvasDesenho.getHeight();
    }
}
