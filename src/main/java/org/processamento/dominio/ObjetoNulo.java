package org.processamento.dominio;

import org.processamento.core.Acao;
import org.processamento.core.Objeto;

import java.util.Set;

public class ObjetoNulo extends Objeto {

    @Override
    protected Set<Ponto> construir() {
        return Set.of();
    }

    @Override
    protected void seProntoParaProcessar(Acao acao) {
    }

    @Override
    protected Objeto adicionarPonto(Ponto ponto) {
        return this;
    }

    @Override
    protected void limparObjetos() {
    }

    @Override
    protected void processarModoPoligono() {

    }

}
