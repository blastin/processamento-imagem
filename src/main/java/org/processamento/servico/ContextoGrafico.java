package org.processamento.servico;

import javafx.scene.canvas.Canvas;
import org.processamento.dominio.Dimensao;
import org.processamento.dominio.Ponto;

public class ContextoGrafico {

    public static void limnparCanvas(Canvas canva) {

        canva
                .getGraphicsContext2D()
                .clearRect(0, 0, canva.getWidth(), canva.getHeight());

    }

    public static void desenharPixelEmCanvas(Canvas canvas, Ponto ponto, Dimensao dimensao) {

        canvas
                .getGraphicsContext2D()
                .fillOval(ponto.xParaInt(), ponto.yParaInt(), dimensao.getComprimento(), dimensao.getAltura());

    }

    private ContextoGrafico() {
    }
}
