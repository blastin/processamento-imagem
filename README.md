
# Projeto de processamento de imagem

Para rodar o projeto

Dependência
1.     Java JDK 11+
2.     git
    
#### Linux / Mac

```sh
    ./gradlew jlink
    build/image/bin/processamento-imagem
```

#### Windows

```sh
    gradlew jlink
    build\image\bin\processamento-imagem
```